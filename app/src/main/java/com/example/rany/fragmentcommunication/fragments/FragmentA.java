package com.example.rany.fragmentcommunication.fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.rany.fragmentcommunication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment implements AdapterView.OnItemClickListener {

    ListView lvArticle;
    String[] articles;
    OnArticleItemClickListener listener;

    public FragmentA() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_a, container, false);
        lvArticle = view.findViewById(R.id.lvArticle);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        articles = getResources().getStringArray(R.array.articles);
        ArrayAdapter<String> articleAdapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_list_item_1, articles
        );
        lvArticle.setAdapter(articleAdapter);
        lvArticle.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(listener != null){
            listener.articleItemClick(articles[position]);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnArticleItemClickListener){
            listener = (OnArticleItemClickListener) context;
        }
    }

    public interface OnArticleItemClickListener{
        void articleItemClick(String title);
    }

}
