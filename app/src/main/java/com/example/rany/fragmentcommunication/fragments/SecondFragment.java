package com.example.rany.fragmentcommunication.fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rany.fragmentcommunication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment implements View.OnClickListener {

    Button btnChange;
    OnViewClickListener listener;

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        btnChange = view.findViewById(R.id.btnChangeText);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnChange.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnViewClickListener){
            listener = (OnViewClickListener) context;
        }
    }

    @Override
    public void onClick(View v) {
        listener.onViewClick("Welcome to Cambodia");
    }

    public interface OnViewClickListener{
        void onViewClick(String str);
    }
}
