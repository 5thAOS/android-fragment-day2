package com.example.rany.fragmentcommunication;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rany.fragmentcommunication.fragments.FirstFragment;

public class CommunicationActivityToFragment extends AppCompatActivity {

    TextView tvShow;
    FirstFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvShow = findViewById(R.id.tvShow);
        tvShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                fragment = FirstFragment.getInstance("first_fragment");
                fragment.setTitle("It is raining now!");
                transaction.add(R.id.container, fragment, "FirstFragment");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }
}
