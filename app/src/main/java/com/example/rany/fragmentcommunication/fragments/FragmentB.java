package com.example.rany.fragmentcommunication.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rany.fragmentcommunication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    TextView tvArticleDetail;

    public FragmentB() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_b, container, false);
        tvArticleDetail = view.findViewById(R.id.tvArticleDetail);
        return view;
    }

    public void getArticleDetail(String detail){
        tvArticleDetail.setText(detail);
    }

}
