package com.example.rany.fragmentcommunication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.rany.fragmentcommunication.fragments.SecondFragment;

public class CommunicationFragmentToActivity extends AppCompatActivity
        implements SecondFragment.OnViewClickListener{

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication_fragment);
        textView = findViewById(R.id.tvResult);
    }

    @Override
    public void onViewClick(String str) {
        textView.setText(str);
    }
}
