package com.example.rany.fragmentcommunication.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rany.fragmentcommunication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    TextView tvFirstFrg;
    private String title;

    public void setTitle(String title){
        this.title = title;
    }

    public FirstFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        tvFirstFrg = view.findViewById(R.id.tvFirstFrg);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvFirstFrg.setText(title);
    }

    public static FirstFragment INSTANCE;
    public static FirstFragment getInstance(String tag){
        if(INSTANCE == null){
            INSTANCE = new FirstFragment();
        }
        Bundle b = new Bundle();
        b.putString("Tag", tag);
        INSTANCE.setArguments(b);

        return INSTANCE;
    }

}
