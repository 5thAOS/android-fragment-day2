package com.example.rany.fragmentcommunication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rany.fragmentcommunication.fragments.FragmentA;
import com.example.rany.fragmentcommunication.fragments.FragmentB;

public class CommunicationFragmentToFragment extends AppCompatActivity implements FragmentA.OnArticleItemClickListener{

    FragmentB frgb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication_fragment_fragment);

        frgb = (FragmentB) getFragmentManager().findFragmentById(R.id.frgB);
    }

    @Override
    public void articleItemClick(String title) {
        frgb.getArticleDetail(title);
    }
}
